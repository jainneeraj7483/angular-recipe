import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipeBookComponent } from './component/recipe-book/recipe-book.component';
import { RecipeDetailComponent } from './component/recipe-detail/recipe-detail.component';
import { AddUpdateRecipeComponent } from './component/add-update-recipe/add-update-recipe.component';

const restaurantRoutes: Routes =
    [

        {
            path: 'recipeBook', component: RecipeBookComponent,
            children:
                [
                    { path: 'addRecipe/:editable/:recipeName', component: AddUpdateRecipeComponent },
                    { path: 'addRecipe', component: AddUpdateRecipeComponent },
                ]
        },
        { path: 'try', component: RecipeDetailComponent },
        { path: '', redirectTo: 'recipeBook', pathMatch: 'full' },
        //{ path: '**', redirectTo: 'recipeBook', pathMatch: 'full' },
    ]
@NgModule
    (
        {
            imports:
                [
                    RouterModule.forRoot(restaurantRoutes)
                ],
            exports:
                [
                    RouterModule
                ]
        }

    )
export class RestaurantRouterModule {

}