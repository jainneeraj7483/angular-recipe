import { Ingredient } from './Ingredeient';

export class RecipeModel {
    name: string;
    description: string;
    price: number;
    ingredients: Ingredient[];

    setName(name: string) {
        this.name = name;
    }
    getName(): string {
        return this.name;
    }
    setDescription(description: string) {
        this.description = description;
    }
    getDescription(): string {
        return this.description;
    }
    setPrice(price: number) {
        this.price = price;
    }
    getPrice(): number {
        return this.price;
    }
    setIngredients(ingredients: Ingredient[]) {
        this.ingredients = ingredients;
    }
    getIngredients(): Ingredient[] {
        return this.ingredients;
    }
    addIngredients(ingredient: Ingredient) {
        this.ingredients.push(ingredient);
    }
}