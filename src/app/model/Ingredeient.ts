export class Ingredient {
    private name: string
    private quantity: number;

    constructor() { }

    setName(name: string) {
        this.name = name;
    }

    getName(): string {
        return this.name;
    }

    setQuantity(quantity: number) {
        this.quantity=quantity;
    }

    getQuantity(): number {
        return this.quantity;
    }

}