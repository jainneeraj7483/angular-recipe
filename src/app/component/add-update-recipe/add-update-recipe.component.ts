import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/service/RecipeService';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-add-update-recipe',
  templateUrl: './add-update-recipe.component.html',
  styleUrls: ['./add-update-recipe.component.css']
})
export class AddUpdateRecipeComponent implements OnInit {

  isRecipeSaved: boolean = false;
  recipeName: string = '';
  constructor(private recipeService: RecipeService, private route: ActivatedRoute) {
  }
  ngOnInit(): void {
    this.isRecipeSaved = this.route.snapshot.params['editable'];
    this.recipeName = this.route.snapshot.params['recipeName'];
    this.route.params.subscribe((params: Params) => {
      this.isRecipeSaved = params['editable'];
      this.recipeName = params['recipeName'];
      console.log("boolean: " + this.isRecipeSaved);
      console.log("name: " + this.recipeName);
    }
    );
  }

}
