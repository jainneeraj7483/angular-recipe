import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RecipeService } from 'src/app/service/RecipeService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipe-book',
  templateUrl: './recipe-book.component.html',
  styleUrls: ['./recipe-book.component.css']
})
export class RecipeBookComponent implements OnInit {

  constructor(private recipeService: RecipeService, private router: Router, private route: ActivatedRoute) {
    console.log("constructor call");
  }

  ngOnInit(): void {
  }

  getRecipeList() {
    return this.recipeService.getRecipeMap();
  }

}
