import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from 'src/app/service/RecipeService';
import { Ingredient } from 'src/app/model/Ingredeient';
import { RecipeModel } from 'src/app/model/RecipeModel';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit, OnChanges {

  ingredients: Ingredient[] = [];
  recipe: RecipeModel = new RecipeModel();

  @Input('saved')
  isRecipeSaved: boolean;
  @Input()
  recipeName: string;

  editRecipeName: boolean = true;
  jhinga: boolean;

  sucessMessage: string = "";
  message: string = "";

  constructor(private recipeService: RecipeService, private route: ActivatedRoute) {
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.jhinga = this.isRecipeSaved;
    console.log("change");
    this.getRecipeDetails();
  }

  ngOnInit(): void {
    console.log("init");
    this.jhinga = this.isRecipeSaved;
    this.getRecipeDetails();
  }

  deleteIngredient(ingredient: Ingredient) {
    this.ingredients.splice(this.ingredients.indexOf(ingredient), 1);
    this.sucessMessage = "";
  }

  addIngredient(ingredientName: HTMLInputElement, ingredientQuantity: HTMLInputElement) {
    if (ingredientName.value && ingredientQuantity.value) {
      let ingredient: Ingredient = new Ingredient();
      ingredient.setName(ingredientName.value);
      ingredient.setQuantity(+ingredientQuantity.value);
      this.ingredients.push(ingredient);
      this.message = "";
      console.log(ingredient);
    }
    else {
      this.message = "Please provide all the inputs for ingredient";
    }
    this.sucessMessage = "";
  }

  saveRecipe(recipeNameInput: HTMLInputElement, recipeDescriptionInput: HTMLInputElement,
    recipePriceInput: HTMLInputElement) {
    if (!(recipeNameInput.value && recipeDescriptionInput.value && recipePriceInput.value)) {
      this.message = "Please provide all the inputs for recipe";
    }
    else if (this.ingredients.length < 1) {
      this.message = "Recipe must atleast have 1 ingredients";
    }
    else {
      if (!this.recipeService.getRecipe(recipeNameInput.value) || !this.editRecipeName) {
        this.recipe.setName(recipeNameInput.value);
        this.recipe.setDescription(recipeDescriptionInput.value);
        this.recipe.setPrice(+recipePriceInput.value);
        this.message = "";
        this.recipe.setIngredients(this.ingredients);
        this.recipeService.addRecipe(this.recipe);
        this.sucessMessage = "Recipe is saved";
        this.recipe = new RecipeModel();
        this.ingredients = [];
      } else {
        this.message = "Recipe Name already exists please use different name";
      }

    }
  }

  editRecipe() {
    this.jhinga = false;
  }

  deleteRecipe() {
    if (this.recipeService.deleteRecipe(this.recipe.getName())) {
      this.message = "Recipe ['" + this.recipe.getName() + "'] deleted successfully";
      this.recipe = undefined;
      this.ingredients = undefined;
    }
  }

  getRecipeDetails() {
    console.log("aaaaaaaaaaaaa");
    console.log(this.isRecipeSaved);
    console.log(this.recipeName);
    if (this.isRecipeSaved) {
      console.log("eeeeeeeeeeeee");
      let recipe: RecipeModel;
      if ((this.recipeName) && (recipe = this.recipeService.getRecipe(this.recipeName))) {
        this.ingredients = recipe.getIngredients();
        this.message = "";
        this.sucessMessage = "";
      }
      else {
        this.message = "Recipe with name: '" + this.recipeName + "' not found";
      }
      this.recipe = recipe;
      this.editRecipeName = false;
    }
  }
}
