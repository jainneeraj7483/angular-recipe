import { RecipeModel } from '../model/RecipeModel';
import { __spreadArrays } from 'tslib';

export class RecipeService {
    recipeMap = new Map<string, RecipeModel>();

    addRecipe(recipe: RecipeModel) {
        this.recipeMap.set(recipe.getName(), recipe);
        console.log(this.recipeMap);
    }

    getRecipe(recipeName: string): RecipeModel {
        return this.recipeMap.get(recipeName);
    }

    deleteRecipe(recipeName: string): boolean {
        return this.recipeMap.delete(recipeName);
    }

    getRecipeMap(): Map<string, RecipeModel> {
        return this.recipeMap;
    }


}