import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { RecipeBookComponent } from './component/recipe-book/recipe-book.component';
import { RecipeDetailComponent } from './component/recipe-detail/recipe-detail.component';
import { ShoppingListComponent } from './component/shopping-list/shopping-list.component';
import { RestaurantRouterModule } from './router-module';
import { RecipeService } from './service/RecipeService';
import { AddUpdateRecipeComponent } from './component/add-update-recipe/add-update-recipe.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecipeBookComponent,
    RecipeDetailComponent,
    ShoppingListComponent,
    AddUpdateRecipeComponent
  ],
  imports: [
    BrowserModule,
    RestaurantRouterModule
  ],
  providers: [RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
